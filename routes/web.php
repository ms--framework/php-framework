<?php

use Illuminate\Support\Facades\Route;
use Ms\Dispatch;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$Dispatch = new Dispatch();


Route::get('/', function () {
    return response()->json(["reponse" => true, "message" => "Bienvenue sur MS-Framework sur LARAVEL"], 200);
});

Route::fallback(function () {
    return response()->json(['Erreur' => 'Page introuvable'], 404);
});
