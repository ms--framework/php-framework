
---
nom : 🚀 Demande de fonctionnalité
à propos : Si vous avez une demande de fonctionnalité 💡
---

**Le contexte**

Qu'essayez-vous de faire et comment voudriez-vous le faire différemment ? Est-ce quelque chose que vous ne pouvez pas faire actuellement ? Est-ce lié à un problème ?

**Comment le résoudre ?**

Les différentes étapes pour la réalisation

**Alternatives**

Pouvez-vous obtenir le même résultat en le faisant d'une manière alternative ? L'alternative est-elle considérable ?

**La fonctionnalité a-t-elle déjà été demandée ?**

Veuillez fournir un lien vers le problème.

**Si la demande de fonctionnalité est approuvée, seriez-vous prêt à soumettre un PR ?**

Oui / Non _(Une aide peut être fournie si vous avez besoin d'aide pour soumettre une PR)_
