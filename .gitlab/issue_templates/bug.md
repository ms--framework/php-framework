
---
nom : 🐜 Rapport de bug
à propos : Si quelque chose ne fonctionne pas 🔧
---

### Quel est le comportement attendu ?

### Quel est le comportement réel ?

### Veuillez fournir un test unitaire qui illustre le beug.

### Autres remarques sur la façon de reproduire le problème ?

### Veuillez fournir des traces des fichiers journaux.

### Des solutions possibles ?

### Pouvez-vous identifier l'emplacement dans le code source où le problème existe ?

### Si le bug est confirmé, accepteriez-vous de soumettre un PR ?

Oui / Non _(Une aide peut être fournie si vous avez besoin d'aide pour soumettre une PR)_
