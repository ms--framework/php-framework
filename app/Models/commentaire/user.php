<?php

namespace App\Models\commentaire;

use Ms\Attribute\DynamoDb\AttributeDefinitions;
use Ms\Attribute\DynamoDb\DynamoDb as AttributeDynamoDb;
use Ms\Attribute\DynamoDb\KeySchema;

#[AttributeDynamoDb(
    TableName: "commentaire_user",
    DatabaseParams: "dynamodb",
    Description: "",
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
)]
class user extends \Ms\Sgbd\DynamoDb
{
    #[KeySchema(GenerateAuto: true)]
    public string $id;

    #[AttributeDefinitions]
    public string $nom;



    /**
     * Condition de supression d'un item
     *
     * @param array $keys
     * @param array $data
     * @return void
     */
    public function Condition_delete_item(array $keys, array $data)
    {
        // dsds
    }

    /**
     * Traitement avant affichage d'information
     *
     * @param array $data
     * @return void
     */
    public function get_Controller(array $data)
    {
        // dsds
    }

    public function Condition_insert_item($data)
    {
        // dsds
    }

    public function after_init()
    {
        // dsds
    }

    /**
     * Traitement avant enregistrement des informations
     *
     * @param array $data
     * @return void
     */
    public function post_Controller(array $data)
    {
        // dsds
    }
}
