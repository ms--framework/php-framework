<?php

namespace App\Models\user;

use Ms\Attribute\DynamoDb\AttributeDefinitions;
use Ms\Attribute\DynamoDb\DynamoDb as AttributeDynamoDb;
use Ms\Attribute\DynamoDb\KeySchema;

#[AttributeDynamoDb(
    TableName: "user_user",
    DatabaseParams: "dynamodb",
    Description: "",
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
)]
class user extends \Ms\Sgbd\DynamoDb
{
    #[KeySchema(GenerateAuto: true)]
    public string $id;

    #[AttributeDefinitions]
    public string $email;

    #[AttributeDefinitions]
    public string $mdp;

    #[AttributeDefinitions]
    public string $nomprenom;

    #[AttributeDefinitions]
    public string $telephone;

    #[AttributeDefinitions]
    public int $profil;

    #[AttributeDefinitions]
    public string $titre;

    #[AttributeDefinitions]
    public string $lien_cv;

    #[AttributeDefinitions]
    public int $nbr_travaux;

    #[AttributeDefinitions]
    public int $competence;

    #[AttributeDefinitions]
    public int $id_system_pays;

    #[AttributeDefinitions]
    public string $note_general;


    public function post_email($data)
    {
        $reponse = $this->scan(request: ["email" => $data]);
        if (count($reponse["result"]) > 0) $this->add_erreur("- Ce email existe déja");
    }

    public function post_mdp($data)
    {
        if (is_numeric($data))
            $this->add_erreur("- Le mot de passe doit être alpha numerique");

        if (strlen($data) < 4)
            $this->add_erreur("- La taille doit être supérieur à 4");
    }


    /**
     * HASH le mot de passe
     */
    public function hash_pwd(string $data): string
    {
        return password_hash($data, PASSWORD_BCRYPT, [
            'cost' => 12
        ]);
    }


    /**
     * Verifie s'il s'agit d'un code
     */
    public function verif_hash_pwd(string $mdp, string $hash): bool
    {
        return password_verify($mdp, $hash);
    }

    public function post_profil($data)
    {
        if (intval($data) > 14) $this->add_erreur("- Ce rôle n'existe pas");
    }

    // public function get_Controller($data)
    // {
    //     return [
    //         "nbr_travaux" => 0,
    //         "note_general" => 0
    //     ];
    // }

    public function get_profil($data)
    {
        $reponse = "Non identifié";
        if (intval($data) == 1) $reponse = "Gérant";
        if (intval($data) == 2) $reponse = "RAF";
        if (intval($data) == 3) $reponse = "Représentant";
        if (intval($data) == 4) $reponse = "Chef de projet";
        if (intval($data) == 5) $reponse = "Fonctionnel";
        if (intval($data) == 6) $reponse = "Architecte Logiciel";
        if (intval($data) == 7) $reponse = "Testeur FrontEnd";
        if (intval($data) == 8) $reponse = "Testeur Backend";
        if (intval($data) == 9) $reponse = "Designer interface graphique";
        if (intval($data) == 10) $reponse = "Intégrateur d'interface graphique Web";
        if (intval($data) == 11) $reponse = "Intégrateur d'interface graphique Mobile";
        if (intval($data) == 12) $reponse = "Dévellopeur Backend";
        if (intval($data) == 13) $reponse = "Business Intelligence";
        if (intval($data) == 14) $reponse = "DevOps";

        return $reponse;
    }

    public function Condition_insert_item($data)
    {
        if (!isset($data["nomprenom"])) $this->add_erreur("- Il doit avoir au moins le nom et prenom");
        if (!isset($data["email"])) $this->add_erreur("- Il doit avoir au moins email");
        if (!isset($data["profil"])) $this->add_erreur("- Il doit avoir au moins le role");
    }

    public function after_init()
    {
        $reponse =  $this->scan(request: []);
        if (empty($reponse["result"])) {
            $soca = $this->insert([
                "nomprenom" => "Admin",
                "mdp" => $this->hash_pwd("123a"),
                "email" => "test@gmail.com",
                "profil" => "1"
            ]);
            if (is_bool($soca)) {
                dd($this->get_erreur());
            }
        }
    }
}
