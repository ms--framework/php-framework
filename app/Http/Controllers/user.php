<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ms\Attribute\RestApi;
use \Ms\Attribute\Route;
use App\Models\user\user as EntityModel;
use Ms\Auth\Jwt;

#[RestApi]
class user extends Controller
{
    public EntityModel $EntityModel;
    public Jwt $Jwt;


    public function __construct()
    {
        $this->EntityModel = new EntityModel();
        $this->Jwt = new Jwt();
    }


    #[Route(path: '/user', method: "GET", description: "Recherche User")]
    public function index(Request $request)
    {
        $reponse = $this->EntityModel->scan(request: $request->all());
        return response()->json($reponse, 200);
    }

    #[Route(path: '/user', method: "POST")]
    public function ajout(Request $request)
    {
        if ($request->has("mdp")) $request->merge(['mdp' => $this->EntityModel->hash_pwd($request->mdp)]);

        $reponse =  $this->EntityModel->insert(data: $request->all());
        return response()->json(["reponse" => true, "id" =>   $reponse], 201);
    }

    #[Route(path: '/user/login', method: "GET", middleware: "Free")]
    public function verif(Request $request)
    {

        $mdp = "";
        $existe = false;
        $data = [];

        if ($request->has("email") & $request->has("mdp")) {
            $mdp = $request->mdp;
            unset($request["mdp"]);
            $reponse = $this->EntityModel->scan(request: $request->all());
            $result = $reponse["result"];

            if (!empty($result)) foreach ($result as $valdep)
                if ($this->EntityModel->verif_hash_pwd($mdp, $valdep["mdp"])) {
                    $existe = true;
                    $data = $valdep;
                }
        }

        if ($existe) {
            $data["Token"] = $this->Jwt->GenerateToken($data);
            return response()->json($data, 200);
        } else {
            return response()->json("Les identifiants sont incorrects", 400);
        }
    }

    #[Route(path: '/user/{id}', method: "PUT")]
    public function update(Request $request, $id)
    {
        if ($request->has("mdp")) $request->merge(['mdp' => $this->EntityModel->hash_pwd($request->mdp)]);
        $reponse = $this->EntityModel->update(keys: $id, data: $request->all());
        return response()->json(["reponse" => $reponse], 201);
    }

    #[Route(path: '/user/{id}', method: "DELETE")]
    public function delete(Request $request, $id)
    {
        $reponse = $this->EntityModel->delete(keys: $id);
        return response()->json(["reponse" => $reponse], 200);
    }
}
