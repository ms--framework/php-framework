<?php

namespace App\Http\Middleware;

use Closure;
use \Ms\Auth\Jwt;
use Illuminate\Http\Request;

class Admin
{
    public Jwt $Jwt;

    public function __construct()
    {
        $this->Jwt = new Jwt();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $auth = false;
        if ($request->hasHeader('Token')) {
            $token = $request->header('Token');
            if (is_bool($this->Jwt->RecupToken($token))) {
                //
            } else {
                $auth = true;
            }
        }

        if ($auth) {
            return $next($request);
        } else {
            return response()->json("- Votre TOKEN est incorrect", 401);
        }
    }
}
