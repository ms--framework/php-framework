<?php

namespace  Ms;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;



class Dispatch
{
    public function __construct()
    {
        $arrive = Request::capture();
        $tab = explode("/", $arrive->getPathInfo());

        if (isset($tab[1])) if ($tab[1] != "") if (class_exists("\App\Http\Controllers\\" . $tab[1] . "")) $this->registerController("\App\Http\Controllers\\" . $tab[1] . "");
    }

    public function registerController(string $controller)
    {
        $middlewareclass = "";
        $class = new \ReflectionClass($controller);

        $clasAtr = $class->getAttributes(\Ms\Attribute\RestApi::class);
        foreach ($clasAtr as $attribute) {
            $routezz = $attribute->newInstance();
            $middlewareclass = $routezz->getMiddleware();
        }

        // Recupere les attribute de la class
        // dd($class->getAttributes());

        foreach ($class->getMethods() as $method) {
            $RouteAttributes = $method->getAttributes(\Ms\Attribute\Route::class);
            if (empty($RouteAttributes)) {
                continue;
            }

            foreach ($RouteAttributes as $routeAttribute) {

                $route = $routeAttribute->newInstance();
                $mapath = $route->getPath();
                $mamethod = strtolower($route->getMethod());
                $maname = $method->getName();
                $middleware = $route->getMiddleware();
                if ($middlewareclass != "Admin" & $middleware == "Admin") $middleware = $middlewareclass;
                Route::$mamethod("$mapath", [$controller, $maname])->middleware($middleware);
            }
        }
    }
}
