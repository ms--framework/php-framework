<?php

namespace  Ms\Sgbd;

use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\Sdk as Aws_SDK;
use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
use Illuminate\Support\Str;
use \Ms\Util\RecupConfig;


class DynamoDb
{
    private array $erreur = [];
    private string $matable;
    private string $origin_matable;
    private array $datatable;
    private array $datakey;
    private array $datattribute;
    private array $data_datos;
    private array $tags;

    public array $bdtemp = [];

    /**
     * My HASH value
     */
    public string $myHASH = "";

    private array $myKeySchema = [];
    private array $myAttribute = [];
    private string $configdb;
    private DynamoDbClient $dynamodb;
    private Marshaler $marshaler;
    private RecupConfig $recupconfig;



    public function __construct()
    {
        $this->marshaler = new Marshaler();
        $this->recupconfig = new RecupConfig();
        $this->getParamsDynamoDb();

        $sdk = new Aws_SDK($this->getDataForDatabase());

        $this->dynamodb = $sdk->createDynamoDb();
    }

    public function getdefinitions()
    {

        $reponse = [];
        $reponse["type"] = "object";
        $reponse["properties"] = [];

        if (!empty($this->myAttribute)) foreach ($this->myAttribute as $val) {
            $reponse["properties"]["$val"] = [
                "type" => "string"
            ];
        }
        $reponse["properties"]["id"] = [
            "type" => "string"
        ];
        return [
            "table" => $this->origin_matable,
            "data" => $reponse
        ];
    }

    /**
     * Ajoute une erreur
     * @param string|array $data Ajoute un message d'alerte
     * @return void
     */
    public function add_erreur(string|array $data): void
    {
        $this->erreur[] = $data;
    }

    /**
     * Creation de item
     *
     * @param array $data
     * @param boolean $VerifPost Par defaut true, demande si on doit faire un traitement de base
     * @return boolean
     */
    public function insert(array $data, bool $VerifPost = true, bool $affiche_erreur = false): bool
    {
        $data["create_at"] = \date("Y-m-d H:i:s");
        $localattribute = $this->myAttribute;
        foreach ($this->myKeySchema as $key => $value) {
            $localattribute[] = $key;
            $existe_key = false;
            foreach ($data as $keys => $values) {
                if ($key == $keys) {
                    $existe_key = true;
                    $this->myHASH = $values;
                }
            }
            if (!$existe_key)
                if ($this->myKeySchema["$key"]["GenerateAuto"])
                    if ($this->myKeySchema["$key"]["KeyType"] == "HASH") {
                        $data["$key"] = Str::uuid()->toString();
                        $this->myHASH = $data["$key"];
                    }
        }

        $nom_method = "Condition_insert_item";
        if (method_exists(get_class($this), $nom_method))
            $this->$nom_method($data);


        if ($VerifPost) {
            foreach ($data as $key => $value) {
                $existe = false;
                $nom_method = "post_" . $key;
                $nom_method_general = "post_Controller";
                if (method_exists(get_class($this), $nom_method))
                    $this->$nom_method($data["" . $key . ""]);

                foreach ($localattribute as $valdep) {
                    if ($key == $valdep) $existe = true;
                    # code...
                }
                if (!$existe) $this->add_erreur("- L'attribut " . $key . " n'existe pas dans le DAO ");

                if (method_exists(get_class($this), $nom_method_general))
                    $this->$nom_method_general($data);
                # code...
            }
        }

        $params = [
            'TableName' => $this->matable,
            'Item' => $this->marshaler->marshalItem($data)
        ];


        if (count($this->get_erreur()) == 0) {
            try {
                $this->dynamodb->putItem($params);
                return $this->myHASH;
            } catch (DynamoDbException $e) {

                $this->add_erreur($e->getMessage());
                $this->add_erreur($params);

                if ($affiche_erreur) {
                    return false;
                } else {
                    response()->json($this->get_erreur(), 400)->send();
                    die();
                }
            }
        } else {
            if ($affiche_erreur) {
                return false;
            } else {
                response()->json($this->get_erreur(), 400)->send();
                die();
            }
        }
    }


    public function recup_data_json($data)
    {
        ob_start();
        include "app/Models/data/$data";
        $doc = ob_get_clean();
        $doc = json_decode($doc, true);
        return $doc;
    }


    /**
     * Supprimer un item
     * @param array|string $keys KeySchema
     * @param array $data  Attribute
     * @param string $ConditionExpression les conditions de traitements
     */
    public function delete(array|string $keys, array $data = [],  string $ConditionExpression = "", bool $affiche_erreur = false): bool
    {

        $eav = [];
        if (!empty($data))  foreach ($data  as $key => $value) {
            $eav[":" . $key . ""] = $value;
        }

        if (is_string($keys)) {
            foreach ($this->myKeySchema as $key => $value) {
                $localattribute[] = $key;
                if ($value["KeyType"] == "HASH") $keys = [
                    $key => $keys
                ];
            }
        }

        $params = [
            'TableName' => $this->matable,
            'Key' => $this->marshaler->marshalItem($keys),

        ];
        if ($ConditionExpression != "") $params["ConditionExpression"] = $ConditionExpression;
        if (!empty($data)) $params["ExpressionAttributeValues"] = $this->marshaler->marshalItem($eav);

        $nom_method = "Condition_delete_item";
        if (method_exists(get_class($this), $nom_method))
            $this->$nom_method($keys, $data);

        if (count($this->get_erreur()) == 0) {
            try {
                $this->dynamodb->deleteItem($params);
                return true;
            } catch (DynamoDbException $e) {
                $this->add_erreur($e->getMessage());
                $this->add_erreur($params);
                if ($affiche_erreur) {
                    return false;
                } else {
                    response()->json($this->get_erreur(), 400)->send();
                    die();
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Mise à jour de item
     *
     * @param array|string $keys KeyScha
     * @param array $data NoKeyAttributions
     * @param string $UpdateExpression la requete à traiter
     * @param string $ConditionExpression Condition de traitement
     * @param boolean $VerifPost  Demande si on doit verifie les valeurs
     * @return boolean
     */
    public function update(array|string $keys, array $data, string $UpdateExpression = "", string $ConditionExpression = "", bool $VerifPost = true, bool $affiche_erreur = false, bool $ignorevide = true): bool
    {
        $data["update_at"] = \date("Y-m-d H:i:s");
        if (isset($data["id"])) unset($data["id"]);
        if (isset($data["__UpdateExpression"])) {
            $UpdateExpression = $data["__UpdateExpression"];
            unset($data["__UpdateExpression"]);
        }

        if (isset($data["__ConditionExpression"])) {
            $ConditionExpression = $data["__ConditionExpression"];
            unset($data["__ConditionExpression"]);
        }

        if ($ignorevide)
            if (!empty($data))
                foreach ($data as $key => $value)
                    if ($value == "")
                        unset($data["$key"]);

        $localattribute = $this->myAttribute;
        $eav = [];
        $req = " set ";
        foreach ($data  as $key => $value) {
            $eav[":" . $key . ""] = $value;
            if ($req == " set ") {
                $req .= " " . $key . " = :" . $key . "";
            } else {
                $req .= " , " . $key . " = :" . $key . " ";
            }
        }

        if (is_string($keys)) {
            foreach ($this->myKeySchema as $key => $value) {
                $localattribute[] = $key;
                if ($value["KeyType"] == "HASH") $keys = [
                    $key => $keys
                ];
            }
        }

        if ($VerifPost) {
            foreach ($data as $key => $value) {
                $existe = false;
                $nom_method = "put_" . $key;
                $nom_method_general = "put_Controller";
                if (method_exists(get_class($this), $nom_method))
                    $this->$nom_method($data["" . $key . ""]);

                foreach ($localattribute as $valdep) {
                    if ($key == $valdep) $existe = true;
                    # code...
                }
                if (!$existe) $this->add_erreur("- L'attribut " . $key . " n'existe pas dans le DAO ");

                if (method_exists(get_class($this), $nom_method_general))
                    $this->$nom_method_general($data);
                # code...
            }
        }

        if ($UpdateExpression != "") $req = $UpdateExpression;

        $params = [
            'TableName' => $this->matable,
            'Key' => $this->marshaler->marshalItem($keys),
            'UpdateExpression' => $req,
            'ExpressionAttributeValues' =>  $this->marshaler->marshalItem($eav),
            'ReturnValues' => 'UPDATED_NEW'
        ];
        if ($ConditionExpression != "") $params["ConditionExpression"] = $ConditionExpression;

        $nom_method = "Condition_update_item";
        if (method_exists(get_class($this), $nom_method))
            $this->$nom_method($keys, $data);

        if (count($this->get_erreur()) == 0) {
            try {
                $this->dynamodb->updateItem($params);
                return true;
            } catch (DynamoDbException $e) {
                $this->add_erreur($e->getMessage());
                $this->add_erreur($params);
                if ($affiche_erreur) {
                    return false;
                } else {
                    response()->json($this->get_erreur(), 400)->send();
                    die();
                }
            }
        } else {
            if ($affiche_erreur) {
                return false;
            } else {
                response()->json($this->get_erreur(), 400)->send();
                die();
            }
        }
    }


    public function format_data(string $key, string  $value): array
    {
        $nom_method = "get_" . $key;

        if (method_exists(get_class($this), $nom_method))
            return [
                "reponse" => true,
                "data" => $this->$nom_method($value)
            ];


        if (substr($key, 0, 3) == "id_") {

            $l_tab = explode("_", $key);

            if (isset($l_tab[2])) if (class_exists("\App\Models\\" . $l_tab[1] . "\\" . $l_tab[2])) {
                $l_class = "\App\Models\\" . $l_tab[1] . "\\" . $l_tab[2];
                $name_class = new $l_class();
                if ($value != "") {

                    if (isset($this->bdtemp["$key"]["$value"])) {
                        return [
                            "reponse" => true,
                            "data" => $this->bdtemp["$key"]["$value"]
                        ];
                    } else {
                        $rep_scan = $name_class->scan(request: ["id" => $value], Format: false);

                        if (is_bool($rep_scan)) {
                            dd($name_class->get_erreur());
                        } else {
                            $this->bdtemp["$key"]["$value"] = $rep_scan["result"];
                            return
                                [
                                    "reponse" => true,
                                    "data" => $rep_scan["result"]
                                ];
                        }
                    }
                }
            }
        }

        return [
            "reponse" => false
        ];
    }

    /**
     * Lance une requete
     *
     * Pour les requêtes venant de la vue
     * _nq_ ::  <>
     * _lt_ :: <
     * _lte :: <=
     * _gt_ :: >
     * _gte :: >=
     * _bw_ :: begin_with
     * _ct_ :: contain
     * _bet :: between
     *
     * __limit pour la limite
     * __projection pour le besoin au niveau de la vue
     * __filter le code de filtre
     * __page le numero de la page
     * __size le nombre par page
     * __select
     *
     * @param string $KeyConditionExpression  La condition d'execution
     * @param array $data les donnes
     * @param array $request La requete venant de la vue
     * @param boolean $Format Demande s'il doit formater
     */
    public function scan(string $FilterExpression = null, array $data = [], string $ProjectionExpression = "", bool $Format = true, array $request = [], bool $affiche_erreur = false, bool $ignorevide = true)
    {
        $eav = [];
        $special = [];

        if ($ignorevide)
            if (!empty($request))
                foreach ($request as $key => $value)
                    if ($value == "")
                        unset($request["$key"]);

        if (!empty($request)) {
            $data = $request;

            foreach ($request  as $key => $value) {
                $comp = substr($key, -4);
                $smk = substr($key, 0, -4);
                $sp = substr($key, 0, 2);
                if ($sp == "__") {
                    $special["$key"] = $value;
                    unset($data["$key"]);
                }
                $tab = ["_nq_", "_lt_", "_lte", "_gt_", "_gte", "_bw_", "_ct_"];
                if (in_array($comp, $tab)) $data["$smk"] = $value;
                // if ($comp != "") $data["$smk"] = $value;
                if ($comp == "_nq_") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " " . $smk . " <> :" . $smk . " ";
                    } else {
                        $FilterExpression .= " and " . $smk . " <> :" . $smk . " ";
                    }
                } else if ($comp == "_bet") {
                    unset($data["$key"]);
                    $tab = explode(":", $value);

                    if (!empty($tab)) if (count($tab) == 2) {
                        $data["" . $smk . "1"] = $tab[0];
                        $data["" . $smk . "1"] = $tab[1];

                        if ($FilterExpression == "") {
                            $FilterExpression = " " . $smk . " between :" . $smk . "1 and :" . $smk . "2 ";
                        } else {
                            $FilterExpression .= " and " . $smk . " between :" . $smk . "1 and :" . $smk . "2 ";
                        }
                    }
                } else  if ($comp == "_lt_") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " " . $smk . " < :" . $smk . " ";
                    } else {
                        $FilterExpression .= " and " . $smk . " < :" . $smk . " ";
                    }
                } else  if ($comp == "_lte") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " " . $smk . " <= :" . $smk . " ";
                    } else {
                        $FilterExpression .= " and " . $smk . " <= :" . $smk . " ";
                    }
                } else  if ($comp == "_gt_") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " " . $smk . " > :" . $smk . " ";
                    } else {
                        $FilterExpression .= " and " . $smk . " > :" . $smk . " ";
                    }
                } else  if ($comp == "_gte") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " " . $smk . " >= :" . $smk . " ";
                    } else {
                        $FilterExpression .= " and " . $smk . " >= :" . $smk . " ";
                    }
                } else  if ($comp == "_bw_") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " begins_with (" . $smk . " ,:" . $smk . ")  ";
                    } else {
                        $FilterExpression .= " and begins_with(" . $smk . " ,:" . $smk . ") ";
                    }
                } else  if ($comp == "_ct_") {
                    unset($data["$key"]);
                    if ($FilterExpression == "") {
                        $FilterExpression = " contains (" . $smk . " ,:" . $smk . ")  ";
                    } else {
                        $FilterExpression .= " and contains (" . $smk . " ,:" . $smk . ") ";
                    }
                } else if (isset($data["$key"])) {
                    $data["$key"] = $value;
                    if ($FilterExpression == "") {
                        $FilterExpression = " " . $key . " = :" . $key . "  ";
                    } else {
                        $FilterExpression .= " and " . $key . " = :" . $key . " ";
                    }
                }
            }
        } else {
            $data["tot"] = "1";
            $FilterExpression = " id <> :tot ";
        }

        foreach ($data  as $key => $value) {
            if (is_numeric($value)) $value = \intval($value);
            if (is_bool($value)) $value = \boolval($value);

            if ($key != "")   $eav[":" . $key . ""] = $value;
        }

        $params = [
            'TableName' => $this->matable,
            // 'ExpressionAttributeNames' => $ean,
        ];



        if ($ProjectionExpression != "") $params["ProjectionExpression"] = $ProjectionExpression;
        if ($FilterExpression != "") $params["FilterExpression"] = $FilterExpression;

        if (!empty($data)) {
            $params["ExpressionAttributeValues"] = $this->marshaler->marshalItem($eav);
        }

        if (isset($special["__limit"]))  $params["Limit"] = $special["__limit"];
        if (isset($special["__filter"]))  $params["FilterExpression"] = $special["__filter"];
        if (isset($special["__projection"])) $params["ProjectionExpression"] = $special["__projection"];
        if (isset($special["__select"]))  $params["Select"] = $special["__select"];



        try {
            $mareponse = [];
            $result = $this->dynamodb->scan($params);

            $result_item = $result["Items"];
            $nbr_item = count($result_item);


            if (isset($special["__page"]) & isset($special["__size"])) {
                $reponse_fi = [];
                $pt = $gd = 1;
                $page = intval($special["__page"]);
                $size = intval($special["__size"]);

                if ($page == 0) {
                    $pt = 0;
                    $gd = $size - 1;
                } else {
                    $pt = $page * $size;
                    $gd = $page * $size + ($size - 1);
                }

                $i = 0;
                if (!empty($result_item)) foreach ($result_item as  $key => $value) {
                    if ($i >= $pt & $i <= $gd) {
                        $reponse_fi["$key"] = $value;
                    }
                    $i++;
                }


                $result_item = $reponse_fi;
            }

            if (!empty($result_item)) {
                foreach ($result_item as $i) {
                    $litdata = $this->marshaler->unmarshalItem($i);
                    if ($Format) {
                        if (!empty($litdata))  foreach ($litdata as $key => $value) {
                            $rp = $this->format_data($key, $value);
                            if ($rp["reponse"])
                                $litdata["format_" . $key . ""] =  $rp["data"];
                        }


                        $nom_method_general = "get_Controller";

                        if (method_exists(get_class($this), $nom_method_general))
                            $litdata["super_format"] =  $this->$nom_method_general($litdata);
                    }
                    $mareponse[] = $litdata;
                }
            }

            $rap_final = [];
            $rap_final["result"] = $mareponse;
            if (isset($special["__page"])) $rap_final["current_page"] = $special["__page"];
            if (isset($special["__size"])) {
                $rap_final["size_page"] = $special["__size"];
                $rap_final["nbr_page"] = ceil($nbr_item / intval($special["__size"]));
            }


            $rap_final["total"] = $nbr_item;
            return  $rap_final;
        } catch (DynamoDbException $e) {
            $this->add_erreur($e->getMessage());
            $this->add_erreur($params);
            if ($affiche_erreur) {
                return false;
            } else {
                response()->json($this->get_erreur(), 400)->send();
                die();
            }
        }
    }

    /**
     * Lance une requete
     * @param string $KeyConditionExpression  La condition d'execution
     * @param array $data les donnes
     * @param boolean $Format Demande s'il doit formater
     */
    public function query(string $KeyConditionExpression, array $data = [], string $ProjectionExpression = "", bool $Format = true, bool $affiche_erreur = false)
    {
        $eav = [];
        $ean = [];



        foreach ($this->myKeySchema as $key => $value) {
            $ean["#" . $key . ""] = $key;
        }

        foreach ($data  as $key => $value) {
            $eav[":" . $key . ""] = $value;
        }

        $params = [
            'TableName' => $this->matable,
            'KeyConditionExpression' => $KeyConditionExpression,
            'ExpressionAttributeNames' => $ean,

        ];

        if ($ProjectionExpression != "") $params["ProjectionExpression"] = $ProjectionExpression;
        if (!empty($data)) $params["ExpressionAttributeValues"] = $this->marshaler->marshalItem($eav);


        try {
            $mareponse = [];
            $result = $this->dynamodb->query($params);


            if (!empty($result["Items"])) {
                foreach ($result['Items'] as $i) {
                    $litdata = $this->marshaler->unmarshalItem($i);
                    if ($Format) {
                        if (!empty($litdata))  foreach ($data as $key => $value) {
                            $rp = $this->format_data($key, $value);
                            if ($rp["reponse"])
                                $litdata["format_" . $key . ""] =  $rp["data"];
                        }

                        $nom_method_general = "get_Controller";
                        if (method_exists(get_class($this), $nom_method_general))
                            $litdata["super_format"] =  $this->$nom_method_general($litdata);
                    }
                    $mareponse[] = $litdata;
                }
            }

            return $mareponse;
        } catch (DynamoDbException $e) {
            $this->add_erreur($e->getMessage());
            $this->add_erreur($params);

            if ($affiche_erreur) {
                return false;
            } else {
                response()->json($this->get_erreur(), 400)->send();
                die();
            }
        }
    }


    /**
     * Lire les données
     *
     * @param array $data lire la donne compose de la HASH et les NoKeyAttribute
     * @param boolean $format Choix si on doit formater
     * @return array|void
     */
    public function read(array $data = [], bool $format = true): array|bool
    {
        $params = [
            'TableName' => $this->matable,
            'Key' => $this->marshaler->marshalItem($data)
        ];

        try {
            $mareponse = [];
            $result = $this->dynamodb->getItem($params);
            if (isset($result["Item"]))   $mareponse = $this->marshaler->unmarshalItem($result["Item"]);

            if ($format) {
                if (!empty($mareponse))  foreach ($mareponse as $key => $value) {
                    $rp = $this->format_data($key, $value);
                    if ($rp["reponse"])
                        $litdata["format_" . $key . ""] =  $rp["data"];
                }

                $nom_method_general = "get_Controller";
                if (method_exists(get_class($this), $nom_method_general))
                    $mareponse["super_format"] =  $this->$nom_method_general($mareponse);
            }
            return $mareponse;
        } catch (DynamoDbException $e) {
            $this->add_erreur($e->getMessage());
            $this->add_erreur($params);

            return false;
        }

        return true;
    }


    /**
     * Recuprer les erreurs
     *
     * @return void
     */
    public function get_erreur(): array
    {
        return $this->erreur;
    }


    /**
     * Générer la liste des tables
     */
    public function lisTable(): array
    {
        $list = $this->dynamodb->listTables();

        return $list["TableNames"];
    }


    /**
     * Creer une table
     *
     * Lien: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.PHP.01.html
     * @return void
     */
    public function createTable(): bool
    {
        $data = $this->data_datos;
        $existe = false;
        $ma_list = $this->lisTable();

        if (!empty($ma_list)) foreach ($ma_list as $valdep)
            if ($valdep == $data["TableName"]) $existe = true;

        if (!$existe) {

            try {
                $this->dynamodb->createTable($data);
                $this->dynamodb->waitUntil('TableExists', [
                    'TableName' => $data["TableName"]
                ]);
                $nom_method_general = "after_init";
                if (method_exists(get_class($this), $nom_method_general))
                    $this->$nom_method_general();



                return true;
            } catch (DynamoDbException $e) {
                $this->add_erreur($e->getMessage());
                $this->add_erreur($data);
                dd($this->get_erreur());
            }
        } else {

            $nom_method_general = "after_init";
            if (method_exists(get_class($this), $nom_method_general))
                $this->$nom_method_general();

            return true;
        }
    }

    public function Recup_Body_Create_BD(\ReflectionClass $class): array
    {
        $reponse = [];
        foreach ($class->getAttributes() as $attribute) {
            $mattri = $attribute->getArguments();
            $this->configdb = $mattri["DatabaseParams"];
            $reponse["TableName"] = $this->recupconfig->GetBrancheData()["database"]["prefix"] . "_" . $this->recupconfig->getData("branche") . "_" . $mattri["TableName"];

            $this->origin_matable = $mattri["TableName"];
            $this->matable = $reponse["TableName"];
            $reponse["BillingMode"] = "PAY_PER_REQUEST";
            $reponse["ProvisionedThroughput"] = [
                "ReadCapacityUnits"
                => $mattri["ReadCapacityUnits"],
                "WriteCapacityUnits" => $mattri["WriteCapacityUnits"]
            ];

            if (getenv("AWS_ACCESS_KEY_ID") != false & (getenv("AWS_SECRET_ACCESS_KEY") != false)) {
                unset($reponse["ProvisionedThroughput"]);
            }
        }

        return $reponse;
    }

    public function Recu_KeySchema_Create_BD(\ReflectionClass $class): array
    {
        $reponse = [];
        foreach ($class->getProperties() as $propreti) {
            $matri = $propreti->getAttributes(\Ms\Attribute\DynamoDb\KeySchema::class);
            if (empty($matri)) {
                continue;
            }

            foreach ($matri as $routeAttribute) {

                $route = $routeAttribute->newInstance();
                $nom_item = $propreti->getName();
                if ($route->getAttributeName() != "") $nom_item = $route->getAttributeName();
                $this->myKeySchema["$nom_item"] = array(
                    "GenerateAuto" => $route->getGenerateAuto(),
                    "KeyType" => $route->getKeyType()
                );
                $reponse[] = array(
                    "AttributeName" => $nom_item,
                    "KeyType"
                    => $route->getKeyType()
                );
            }
        }

        foreach ($class->getProperties() as $propreti) {
            $matri = $propreti->getAttributes(\Ms\Attribute\DynamoDb\AttributeDefinitions::class);
            if (empty($matri)) {
                continue;
            }

            foreach ($matri as $routeAttribute) {
                $route = $routeAttribute->newInstance();
                $nom_item = $propreti->getName();
                if ($route->getAttributeName() != "") $nom_item = $route->getAttributeName();
                $this->myAttribute[] = $nom_item;
            }
        }
        $this->myAttribute[] = "create_at";
        $this->myAttribute[] = "update_at";

        return $reponse;
    }




    public function Recup_Attribution_Create_BD(\ReflectionClass $class): array
    {
        $reponse = [];

        foreach ($class->getProperties() as $propreti) {
            $matri = $propreti->getAttributes(\Ms\Attribute\DynamoDb\KeySchema::class);
            if (empty($matri)) {
                continue;
            }

            foreach ($matri as $routeAttribute) {

                $route = $routeAttribute->newInstance();
                $nom_item = $propreti->getName();
                $type_item_method = $propreti->getType()->getName();

                $type_item = "S";
                if ($type_item_method == "int")
                    $type_item = "N";

                if ($route->getAttributeName() != "") $nom_item = $route->getAttributeName();

                $reponse[] = array(
                    "AttributeName" => $nom_item,
                    "AttributeType"
                    => $type_item

                );
            }
        }



        return $reponse;
    }

    /**
     * Récupere les paramètres de la classe
     *
     * @return array
     */
    public function getParamsDynamoDb(): array
    {
        $reponse = [];
        $class = new \ReflectionClass($this);
        $this->datatable = $reponse = $this->Recup_Body_Create_BD($class);
        $this->datakey =  $reponse["KeySchema"] = $this->Recu_KeySchema_Create_BD($class);
        $this->datattribute = $reponse["AttributeDefinitions"] = $this->Recup_Attribution_Create_BD($class);
        $reponse["Tags"] =  $this->recupconfig->getData("Tags_aws");
        $this->data_datos = $reponse;
        return $reponse;
    }

    /**
     * Initalise les bases de données dynamodb
     */
    public function init_bd()
    {

        $dossier = opendir("../app/Models");
        while (false !== ($fichier = readdir($dossier))) {

            $last_variable = substr($fichier, -4);
            if ($last_variable == ".php") {
                $nom_class = "App\Models\\" . substr($fichier, 0, -4);

                $classtemp = new $nom_class();
                if (method_exists($classtemp, "createTable"))  if (!$classtemp->createTable()) {

                    dd($classtemp->get_erreur());
                }
            }

            if (is_dir("../app/Models/$fichier")) {
                if (strlen($fichier) > 2) {
                    $dossiers = opendir("../app/Models/$fichier");
                    while (false !== ($fichiers = readdir($dossiers))) {
                        $last_variables = substr($fichiers, -4);
                        if ($last_variables == ".php") {
                            $nom_class = "App\Models\\" . $fichier . "\\" . substr($fichiers, 0, -4);
                            $classtemp = new $nom_class();
                            if (method_exists($classtemp, "createTable"))  if (!$classtemp->createTable()) {
                                dd($classtemp->get_erreur());
                            }
                        }
                    }
                }
            }
        }
        return true;
    }



    /**
     * Recupere les coordonnées de la BD
     *
     * @return array
     */
    public function getDataForDatabase(): array
    {
        $data = [];

        try {
            $data = $this->recupconfig->GetBrancheData()["database"]["" . $this->configdb . ""];
        } catch (\Throwable $th) {
            dd("Je récupere pas les données de configuration ");
        }

        if (getenv("AWS_ACCESS_KEY_ID") != false & (getenv("AWS_SECRET_ACCESS_KEY") != false)) {
            unset($data["endpoint"]);
            if (isset($data["ProvisionedThroughput"]))    unset($data["ProvisionedThroughput"]);
            $data["credentials"] = [
                'key' => getenv("AWS_ACCESS_KEY_ID"),
                'secret' => getenv("AWS_SECRET_ACCESS_KEY")
            ];
        }



        return $data;
    }
}
