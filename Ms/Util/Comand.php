<?php

namespace Ms\Util;

use Composer\Script\Event;
use Symfony\Component\Yaml\Yaml;
use \Ms\Util\RecupConfig;

class Comand
{

    public static function swagger(Event $event)
    {
        $includeFiles = require 'vendor/composer/autoload_files.php';
        foreach ($includeFiles as $file) {
            require $file;
        }

        function recherchecontroller(): array
        {
            $reponse = [];

            $dossier = opendir("app/Http/Controllers");

            while (false !== ($fichier = readdir($dossier))) {

                if (is_file("app/Http/Controllers/" . $fichier) & ($fichier != "Controller.php")) {
                    $name_controller = substr($fichier, 0, -4);
                    $description_controller = "";
                    $lien_class = "\App\Http\Controllers\\" . $name_controller;
                    $myclass = new $lien_class();
                    $class = new \ReflectionClass("\App\Http\Controllers\\" . $name_controller);
                    $clasAtr = $class->getAttributes(\Ms\Attribute\RestApi::class);
                    if (!empty($clasAtr)) foreach ($clasAtr as $attribute) {
                        $routezz = $attribute->newInstance();
                        $n_cot = $routezz->getName();
                        $description_controller = $routezz->getDescription() . ". Midlleware:" . $routezz->getMiddleware();
                        if ($n_cot != "") $name_controller = $n_cot;
                    }

                    //
                    $reponse["tags"][] = [
                        "name" => $name_controller,
                        "description" => $description_controller
                    ];
                    //


                    //
                    $rep_defin = $myclass->EntityModel->getdefinitions();
                    $reponse["definitions"]["" . $rep_defin["table"] . ""]
                        = $rep_defin["data"];
                    //


                    foreach ($class->getMethods() as $method) {
                        $RouteAttributes = $method->getAttributes(\Ms\Attribute\Route::class);
                        if (empty($RouteAttributes)) {
                            continue;
                        }

                        foreach ($RouteAttributes as $routeAttribute) {
                            $route = $routeAttribute->newInstance();
                            $mapath = $route->getPath();
                            $mamethod = strtolower($route->getMethod());
                            $output = $route->getOutput();
                            $input = $route->getInput();
                            $inputPath = $route->getInputPath();
                            $inputformData = $route->getinputFormData();

                            $reponse["paths"]["$mapath"]["$mamethod"] = [];
                            $reponse["paths"]["$mapath"]["$mamethod"]["tags"][] = $name_controller;
                            $reponse["paths"]["$mapath"]["$mamethod"]["summary"] = $route->getTitre();
                            $reponse["paths"]["$mapath"]["$mamethod"]["description"] = $route->getDescription();

                            //
                            if ($output == "") {
                                $reponse["paths"]["$mapath"]["$mamethod"]["responses"]["200"]["schema"]
                                    = ['$ref' => "#/definitions/" . $rep_defin["table"] . ""];
                            } else {
                                $reponse["paths"]["$mapath"]["$mamethod"]["responses"]["200"]["description"] = "";
                                $reponse["paths"]["$mapath"]["$mamethod"]["responses"]["200"]["content"]["application/json"]["schema"]["type"] = "object";
                                $reponse["paths"]["$mapath"]["$mamethod"]["responses"]["200"]["content"]["application/json"]["schema"]["properties"] = [];
                                $tabz = explode(",", $output);
                                if (!empty($tabz)) foreach ($tabz as $zz) {
                                    $reponse["paths"]["$mapath"]["$mamethod"]["responses"]["200"]["content"]["application/json"]["schema"]["properties"]["$zz"] = ["type" => "string"];
                                }
                            }
                            //

                            //
                            if ($inputformData != "") {
                                $tabz = explode(",", $inputformData);
                                if (!empty($tabz)) foreach ($tabz as $zz) {
                                    $reponse["paths"]["$mapath"]["$mamethod"]["parameters"][] = [
                                        "name" => $zz,
                                        "in" => "formData",
                                        "type" => "file"
                                    ];
                                }
                            }
                            //

                            if ($input != "") {
                                $tabz = explode(",", $input);
                                if (!empty($tabz)) foreach ($tabz as $zz) {

                                    if ($mamethod == "get") {
                                        $reponse["paths"]["$mapath"]["$mamethod"]["parameters"][] = [
                                            "name" => $zz,
                                            "in" => "query",
                                            "schema" => ["type" => "string"]
                                        ];
                                    } else {
                                        $reponse["paths"]["$mapath"]["$mamethod"]["parameters"][] = [
                                            "name" => "body",
                                            "in" => "body",
                                            "schema" => [
                                                "type" => "object",
                                                "properties" => ["$zz" => [
                                                    "type" => "string"
                                                ]]
                                            ]
                                        ];
                                    }
                                }
                            }
                            //

                            if ($inputPath != "") {
                                $tabz = explode(",", $inputPath);
                                if (!empty($tabz)) foreach ($tabz as $zz) {
                                    $reponse["paths"]["$mapath"]["$mamethod"]["parameters"][] = [
                                        "name" => $zz,
                                        "in" => "path",
                                        "type" => "string"
                                    ];
                                }
                            }
                            //
                            if ($input == "" & $inputPath == "" & $inputformData == "") {
                                if ($mamethod == "get") {
                                    $tab_rech = $rep_defin["data"]["properties"];
                                    if (!empty($tab_rech)) foreach ($tab_rech as $keyzz => $valuezz) {
                                        $reponse["paths"]["$mapath"]["$mamethod"]["parameters"][] = [
                                            "name" => $keyzz,
                                            "in" => "query",
                                            "schema" => ["type" => "string"]
                                        ];
                                    }
                                } else {
                                    $reponse["paths"]["$mapath"]["$mamethod"]["parameters"][] = [
                                        "name" => "body",
                                        "in" => "body",
                                        "schema" => [
                                            '$ref' => "#/definitions/" . $rep_defin["table"] . ""
                                        ]
                                    ];
                                }
                            }
                        }
                    }
                }
            }

            return $reponse;
        }

        $recup_body = Yaml::parseFile("Ms/Example/swagger");
        $recupconfig = new RecupConfig();

        $recup_body["info"]["description"] = $recupconfig->getData("description") . ". Dev by " . $recupconfig->getData("dev_by");
        $recup_body["info"]["title"] = $recupconfig->getData("nom");
        $recup_body["info"]["contact"] = $recupconfig->getData("contact");
        $recup_body["schemes"] = [];
        $recup_body["schemes"][] = $recupconfig->getData("url_dev");
        $recup_body["schemes"][] = $recupconfig->getData("url_prod");
        $zoo = recherchecontroller();
        $recup_body["tags"] = $zoo["tags"];
        $recup_body["definitions"] = $zoo["definitions"];
        $recup_body["paths"] = $zoo["paths"];
        $new_yaml = Yaml::dump($recup_body);
        file_put_contents("public/swagger/oapi.yml", $new_yaml);
        dd('reussi');
    }

    public static function createmodel(Event $event)
    {
        $compo = $event->getArguments();
        $groupe = $compo[0];
        $compte = $compo[1];

        if (!file_exists("app/Models/" . $groupe . ""))
            mkdir("app/Models/" . $groupe . "");

        $route_model = "app/Models/" . $groupe . "/" . $compte . ".php";

        $confdoc = fopen($route_model, 'a');

        $config_doc = file_get_contents("Ms/Example/Model");
        $config_doc = str_replace("MONGROUPE", $groupe, $config_doc);
        $config_doc = str_replace("MONCOMPTE", $compte, $config_doc);

        fputs($confdoc, $config_doc);
        fclose($confdoc);

        echo "creer MODEL";
    }


    public static function createcontroller(Event $event)
    {
        $compo = $event->getArguments();
        $controller = $compo[0];
        $model = $compo[1];

        $route_model = "app/Http/Controllers/" . $controller . ".php";

        $confdoc = fopen($route_model, 'a');

        $config_doc = file_get_contents("Ms/Example/Controller");
        $config_doc = str_replace("MONCONTROLLER", $controller, $config_doc);
        $config_doc = str_replace("MONMODEL", $model, $config_doc);

        fputs($confdoc, $config_doc);
        fclose($confdoc);

        echo "creer CONTROLLER";
    }

    public static function config_dynamo(Event $event)
    {
        session_start();
        $compo = $event->getArguments();
        $branche = $compo[0];

        $_ENV["branche"] = $branche;

        \Ms\Util\Comand::init_bd();
    }

    /**
     * Initialise la base de donnée
     * @return [type] [description]
     */
    public static function init_bd()
    {

        $includeFiles = require 'vendor/composer/autoload_files.php';
        foreach ($includeFiles as $file) {
            require $file;
        }

        $dossier = opendir("app/Models");

        while (false !== ($fichier = readdir($dossier))) {


            $last_variable = substr($fichier, -4);
            if ($last_variable == ".php") {
                $nom_class = "\App\Models\\" . substr($fichier, 0, -4);

                $classtemp = new $nom_class();
                try {
                    if (!$classtemp->createTable()) {
                        dd($classtemp->get_erreur());
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }


            if (is_dir("app/Models/$fichier")) {
                if (strlen($fichier) > 2) {

                    $dossiers = opendir("app/Models/$fichier");
                    while (false !== ($fichiers = readdir($dossiers))) {

                        $last_variables = substr($fichiers, -4);
                        if ($last_variables == ".php") {
                            $nom_class = "\App\Models\\" . $fichier . "\\" . substr($fichiers, 0, -4);
                            $classtemp = new $nom_class();

                            try {
                                if (!$classtemp->createTable()) {
                                    dd($classtemp->get_erreur());
                                }
                            } catch (\Exception $th) {
                                dd($th);
                            }
                        }
                    }
                }
            }
        }

        echo ' Base de donnee init ';
    }
}
