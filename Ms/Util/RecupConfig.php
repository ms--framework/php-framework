<?php

namespace Ms\Util;

use Symfony\Component\Yaml\Yaml;

class RecupConfig
{

    /**
     * Donnee config de l'appli
     */
    public array $madata;

    public function __construct()
    {
        $this->get();
        $this->InitGetenv();
    }

    /**
     * Récupere les données
     *
     * @param string $data
     * @return void
     */
    public function get(string $data = null)
    {
        $ma_reponse = false;
        $route = "config.yaml";
        if (is_file("config.yaml")) $route = "config.yaml";
        if (is_file("../config.yaml")) $route = "../config.yaml";
        $reponse = Yaml::parseFile($route);
        if (is_null($data)) {
            $ma_reponse = $reponse;
        }
        if (!is_null($data) & isset($reponse["data"])) $ma_reponse = $reponse["data"];

        $this->madata = $ma_reponse;
    }

    public function getData(string $key = null)
    {
        if (!is_null($key)) {
            if (isset($this->madata["$key"])) return $this->madata["$key"];
        } else {
            return $this->madata;
        }
    }

    /**
     * Ajoute les variables d'environnement
     *
     * @param array $data
     * @return array
     */
    public function InitGetenv(): array
    {
        $data = $this->madata;
        $variableenvironnement = getenv();

        if (is_array($variableenvironnement)) foreach ($variableenvironnement as $key => $value) {


            $tab = explode(".", $key);
            $longeur = count($tab);
            if ($longeur == 1) $data["$key"] = $value;
            if ($longeur == 2) $data["" . $tab[0] . ""]["" . $tab[1] . ""] = $value;
            if ($longeur == 3) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""] = $value;
            if ($longeur == 4) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""]["" . $tab[3] . ""] = $value;
            if ($longeur == 5) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""]["" . $tab[3] . ""]["" . $tab[4] . ""] = $value;
            if ($longeur == 6) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""]["" . $tab[3] . ""]["" . $tab[4] . ""]["" . $tab[5] . ""] = $value;
        }

        $variableenvironnement = $_ENV;
        if (is_array($variableenvironnement)) foreach ($variableenvironnement as $key => $value) {
            $tab = explode(".", $key);
            $longeur = count($tab);
            if ($longeur == 1) $data["$key"] = $value;
            if ($longeur == 2) $data["" . $tab[0] . ""]["" . $tab[1] . ""] = $value;
            if ($longeur == 3) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""] = $value;
            if ($longeur == 4) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""]["" . $tab[3] . ""] = $value;
            if ($longeur == 5) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""]["" . $tab[3] . ""]["" . $tab[4] . ""] = $value;
            if ($longeur == 6) $data["" . $tab[0] . ""]["" . $tab[1] . ""]["" . $tab[2] . ""]["" . $tab[3] . ""]["" . $tab[4] . ""]["" . $tab[5] . ""] = $value;
        }

        return $this->madata = $data;
    }

    /**
     * Récupere les données d'une branche
     *
     * @param string $key
     * @return void
     */
    public function GetBrancheData(string $key = null)
    {

        $ma_reponse = false;
        $data = $this->madata;
        if (isset($data["branche"]) & $data["branche"] != "")
            if (!is_null($key)) {
                $ma_reponse = $data["" . $data["branche"] . ""]["$key"];
            } else {
                $ma_reponse = $data["" . $data["branche"] . ""];
            }
        return $ma_reponse;
    }
}
