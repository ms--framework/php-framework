<?php

namespace Ms\Auth;

use \Firebase\JWT\JWT as AuthJwt;
use \Ms\Util\RecupConfig;

class Jwt
{

    public RecupConfig $recupconfig;
    private string $key;

    public function __construct()
    {
        $this->recupconfig = new RecupConfig();
        $this->key = $this->recupconfig->getData("jwt_secret");
    }


    /**
     * Genere le token
     *
     * @param array $data
     * @return string
     */
    public function GenerateToken(array $data): string
    {
        $data["date_Token"] = \date("Y-m-d H:i:s-v");
        return AuthJwt::encode($data, $this->key);
    }

    /**
     * Verifie le token
     */
    public function RecupToken(string $Token): bool | array
    {
        try {
            $decoded = AuthJwt::decode($Token, $this->key, array('HS256'));
            return (array)$decoded;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
