<?php

namespace  Ms\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class Route
{
    public function __construct(
        private string $path,
        private string $method = "GET",
        private string $input = "",
        private string $inputPath = "",
        private string $inputformData = "",
        private string $output = "",
        private string $format = "application/json",
        private string $middleware = "Admin",
        private string $description = "",
        private string $titre = ""
    ) {
        // Do nothing
    }

    public function getInput(): string
    {
        return $this->input;
    }

    public function getInputPath(): string
    {
        return $this->inputPath;
    }

    public function getinputFormData(): string
    {
        return $this->inputformData;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function getOutput(): string
    {
        return $this->output;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getMiddleware(): string
    {
        return $this->middleware;
    }
}
