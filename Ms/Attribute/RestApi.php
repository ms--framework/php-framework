<?php

namespace  Ms\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class RestApi
{
    public function __construct(private string $middleware = "Admin", private string $name = "", private string $description = "")
    {
        // Do nothing
    }

    public function getMiddleware(): string
    {
        return $this->middleware;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
