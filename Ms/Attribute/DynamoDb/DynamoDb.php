<?php

namespace  Ms\Attribute\DynamoDb;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class DynamoDb
{
    public function __construct(private string $TableName, private string $Description, private string $DatabaseParams, private int $ReadCapacityUnits = 1, private int $WriteCapacityUnits = 1)
    {
        // Do nothing
    }
}
