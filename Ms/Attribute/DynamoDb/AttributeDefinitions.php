<?php

namespace  Ms\Attribute\DynamoDb;

use Attribute;

/**
 * Lien: https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_AttributeValue.html
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
class AttributeDefinitions
{

    public function __construct(private string $AttributeName = '', private string $AttributeType = '', private string $Description = "")
    {
        // Do nothing
    }


    public function getAttributeName()
    {
        return $this->AttributeName;
    }

    public function getAttributeType()
    {
        return $this->AttributeType;
    }
}
