<?php

namespace  Ms\Attribute\DynamoDb;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class KeySchema
{

    public function __construct(private string $AttributeName = "", private string $KeyType = 'HASH', private bool $GenerateAuto = true)
    {
        // Do nothing
    }

    public function getAttributeName()
    {
        return $this->AttributeName;
    }

    public function getKeyType()
    {
        return $this->KeyType;
    }

    public function getGenerateAuto()
    {
        return $this->GenerateAuto;
    }
}
